/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.util.Collection;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ALEXANDER
 */
public class Principal {

    public static void main(String[] args) {
        int op;
        Scanner leer = new Scanner(System.in);
        System.out.println("Introduce la opción deseada:"
                + "\n 1.- Insertar Producto"
                + "\n 2.- Insertar Orden"
                + "\n 3.- Insertar Linea Orden"
                + "\n 4.- Ver productos"
                + "\n 5.- Salir");
        op = leer.nextInt();
        Metodos obj = new Metodos();
        do {
            switch (op) {
                case 1:
                    String nomb;
                    float precio;
                    System.out.println("Nombre: ");
                    nomb = leer.next();
                    System.out.println("Precio");
                    precio = leer.nextFloat();

                    obj.InsertaProdcuto(nomb, precio);
                    break;
                case 2:
                    obj.InsertaOrden();
                    break;
                case 3:
                    int cant;
                    System.out.println("Cantidad: ");
                    cant = leer.nextInt();
                    obj.InsertaLineaOrden(cant);
                    break;
                case 4:
                    EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
                    EntityManager em = emf.createEntityManager();
                    em.getTransaction().begin();
                    int op2;
                    do {
                        System.out.println("Introduce la opción deseada:"
                                + "\n 1.- Ver Producto"
                                + "\n 2.- Ver Orden"
                                + "\n 3.- Ver Linea Orden"
                                + "\n 4.- Salir");

                        op2 = leer.nextInt();
                        if (op2 == 1) {
                            Collection<Producto> producto;
                            producto = em.createNamedQuery("Producto.findAll").getResultList();
                            for (Producto pro : producto) {
                                System.out.println("Id: " + pro.getIdProducto() + " Descripcion: " + pro.getDescripcion() + " Precio: " + pro.getPrecio());
                            }
                        }
                        if (op == 2) {
                            Collection<Orden> orden;
                            orden = em.createNamedQuery("Orden.findAll").getResultList();
                            for (Orden or : orden) {
                                System.out.println("Id Orden: " + or.getIdOrden() + " Fecha Orden: " + or.getFechaOrden());
                            }
                        }
                        if (op == 3) {
                            Collection<Lineaorden> liOrden;
                            liOrden = em.createNamedQuery("Lineaorden.findAll").getResultList();
                            for (Lineaorden lio : liOrden) {
                                System.out.println("Id Orden: " + lio.getOrden() + " Id producto: " + lio.getProducto() + " Cantidad: " + lio.getCantidad());
                            }
                        }
                    } while (op != 4);
                    try {
                        em.getTransaction().commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    em.close();
                    emf.close();
                    break;
                default:
                    System.out.println("Opción no valida");
            }
        } while (op != 5);
    }
}
