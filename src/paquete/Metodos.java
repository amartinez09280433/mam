package paquete;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Metodos {

    Producto prod = new Producto();
    Orden orden = new Orden();

    public void InsertaProdcuto(String producto, Float precio) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosAMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        prod.setDescripcion(producto);
        prod.setPrecio(precio);
        try {
            em.persist(prod);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        em.close();
        emf.close();
    }

    public void InsertaOrden() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosAMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        java.util.Date tiempo = new java.util.Date();
        java.sql.Date fecha = new java.sql.Date(tiempo.getTime());
        Orden or = new Orden();
        or.setFechaOrden(fecha);

        try {
            em.persist(or);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        em.close();
        emf.close();
    }

    public void InsertaLineaOrden(int cantidad) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosAMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Lineaorden lor = new Lineaorden(orden.getIdOrden(), prod.getIdProducto());
        lor.setCantidad(cantidad);
        try {
            em.persist(lor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        em.close();
        emf.close();
    }
}
